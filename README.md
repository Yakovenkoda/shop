# Инструкция #
___
### Инструменты ###
* [Git bash ](https://git-scm.com/downloads)
* [Denver](http://www.denwer.ru/) или [xampp](https://www.apachefriends.org/ru/index.html)
___
### Руководство ###
После установки локального сервера (**Denver/xampp**) с помощью  **Git bash** клонировать данный репозиторий (git clone https://Yakovenkoda@bitbucket.org/Yakovenkoda/shop.git) в нужную директорию в зависимости от настроек локального сервера (пример: http://localhost/Shop/). Перед запуском распаковать базу данных файл **shopdrive.sql**.
___
### Настройки / Компоненты ###
* Дамп базы данных находится в файле **shopdrive.sql** бд MySQL
* Настройки подключения к базе в файле **config.xml** - там же Контактные данные
* использовался шаблон Model-View-Controller
* визуальные компоненты Bootstrap
* константы маршрутизации (URL,SITE_PATH) в index.php